package com.example.home.worldcurrencies.data.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.home.worldcurrencies.R;
import com.example.home.worldcurrencies.model.Currency;
import com.example.home.worldcurrencies.ui.activity.CurrencyInformationActivity;
import com.example.home.worldcurrencies.ui.activity.MainActivity;

import java.util.List;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.ViewHolder> {

    List<Currency> currencies = MainActivity.getArrayCurrency();
    Context context;

    public CurrencyAdapter(Context context) {
        this.currencies = currencies;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_currency, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name.setText(currencies.get(position).getName());
        holder.icon.setImageResource(currencies.get(position).getIcon());
        holder.description.setText(currencies.get(position).getDescription());

    }

    @Override
    public int getItemCount() {
        return currencies.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView description;
        ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_currency_name);
            description = itemView.findViewById(R.id.item_currency_description);
            icon = itemView.findViewById(R.id.item_currency_icon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CurrencyInformationActivity.start();
                }
            });
        }
    }
}
