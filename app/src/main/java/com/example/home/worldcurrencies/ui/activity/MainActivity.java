package com.example.home.worldcurrencies.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.home.worldcurrencies.R;
import com.example.home.worldcurrencies.data.adapter.CurrencyAdapter;
import com.example.home.worldcurrencies.model.Currency;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    static ArrayList<Currency> currencies = new ArrayList<>();
    public static final String KEY = "key";
    CurrencyAdapter adapter;
    RecyclerView currencyList;


    static {
        currencies.add(new Currency(R.mipmap.ic_launcher_round, "usd", "The United States dollar"));
        currencies.add(new Currency(R.mipmap.ic_launcher_round, "ru", "ruble"));
        currencies.add(new Currency(R.mipmap.ic_launcher_round, "eur", "euro"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        currencyList = findViewById(R.id.currency_list);
        adapter = new CurrencyAdapter(this);
        currencyList.setLayoutManager(new LinearLayoutManager(this));
        currencyList.setAdapter(adapter);


    }

    public static ArrayList<Currency> getArrayCurrency() {
        return currencies;
    }
}
