package com.example.home.worldcurrencies.retrofit;

import com.example.home.worldcurrencies.model.Currency;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public class Retrofit {

    private static final String ENDPOINT = "http://data.fixer.io/api";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface{

        @GET("/latest")
        void getCurrency(@Query("access_key") String accessKey,
                         @Query("symbols") String symbols,
                           Callback<Currency> callback);

    }

    private static void initialize(){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getOneCurrency(String accessKey,String symbols,  Callback<Currency> callback){
        apiInterface.getCurrency(accessKey, symbols, callback);
    }
}
