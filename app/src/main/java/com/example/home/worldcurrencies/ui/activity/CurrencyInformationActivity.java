package com.example.home.worldcurrencies.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;

import com.example.home.worldcurrencies.R;
import com.example.home.worldcurrencies.model.Currency;
import com.example.home.worldcurrencies.retrofit.Retrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CurrencyInformationActivity extends AppCompatActivity {

    String aceesToken = "eb4dae9efd8aa19b5810a8a5c62b7e8e";
    ArrayList<Currency> arrCurrency = MainActivity.getArrayCurrency();
    ArrayList<Currency> changeArrCurrency = new ArrayList<>();
    Currency currencyGetExtra;

    public static void start(Context context, Currency currency) {
        Intent intent = new Intent(context, CurrencyInformationActivity.class);
        intent.putExtra(MainActivity.KEY, currency);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.currency_information_activity);
        //getIntent().getSerializableExtra("");
        currencyGetExtra = (Currency) getIntent().getSerializableExtra(MainActivity.KEY);


        //String nameCurrency = "USD";

        getInfromationAboutCource();


    }

    private void getInfromationAboutCource() {

        for (Currency myCurrency : arrCurrency) {
            if(!myCurrency.getName().equals(currencyGetExtra.getName())){

                Retrofit.getOneCurrency(aceesToken, myCurrency.getName(), new Callback<Currency>() {
                    @Override
                    public void success(Currency currency, Response response) {
                        changeArrCurrency.add(currency);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }
        }

    }
}
