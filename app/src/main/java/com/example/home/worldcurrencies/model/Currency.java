package com.example.home.worldcurrencies.model;

import com.google.gson.annotations.SerializedName;

public class Currency {
    private int icon;
    private String name;
    private String description;
    @SerializedName("")
    private String cource;

    public Currency(int icon, String name, String description) {
        this.icon = icon;
        this.name = name;
        this.description = description;
    }

    public int getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
